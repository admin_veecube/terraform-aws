resource "aws_security_group" "mywebsg" {
  name = "mywebsg"
  description = "Alooing port 80 and port 22"
  ingress {
    from_port = 80
    protocol  = "tcp"
    to_port   = 80
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 22
    protocol  = "tcp"
    to_port   = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    protocol  = "-1"
    to_port   = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

}

resource "aws_instance" "mywebec2" {
  ami = "ami-04902260ca3d33422"
  instance_type = "t2.micro"
  user_data = <<-EOF
     #!/bin/bash
     yum -y install httpd
     echo "Hello World" > /var/www/html/index.html
     service httpd start
     systemctl enable httpd
     EOF
  vpc_security_group_ids = [
   aws_security_group.mywebsg.id
  ]
  tags = {
    Name = "Webinstance"
  }



}
output "myip" {
  value = [ aws_instance.mywebec2.private_ip, aws_instance.mywebec2.public_ip ]

}
