# Security Group
# Create EC2 Instance
# Create ELB
resource "aws_security_group" "web_sg" {

  # from External to EC2 instance
  ingress {
    from_port = 22
    protocol  = "tcp"
    to_port   = 22
    cidr_blocks = ["0.0.0.0/0"]

  }

  ingress {
    from_port = 80
    protocol  = "tcp"
    to_port   = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  # from ec2 instance to anywhere
  egress {
    from_port = 0
    protocol  = "-1"
    to_port   = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}


resource "aws_instance" "myec2" {
  ami = "ami-04902260ca3d33422"
  count = 2
  vpc_security_group_ids = [aws_security_group.web_sg.id]
  instance_type = "t2.micro"
  availability_zone = "us-east-1a"
  user_data = <<-EOF
    #!/bin/bash
    yum -y install httpd
    echo "Testing LB using Terraform" > /var/www/html/index.html
    service httpd start
    systemctl enable httpd
  EOF
}

resource "aws_elb" "myelb" {
  availability_zones = ["us-east-1a", "us-east-1b"]
  security_groups = [aws_security_group.web_sg.id]

  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  instances = [aws_instance.myec2.0.id, aws_instance.myec2.1.id]
}
