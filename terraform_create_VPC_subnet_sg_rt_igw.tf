## Create VPC
resource "aws_vpc" "myvpc" {
  cidr_block = "172.16.0.0/16"
  enable_dns_hostnames = "true"
  enable_dns_support = "true"
  tags = {
    Name = "myvpc"
  }
}

## Create Subnets Private/Public
 resource "aws_subnet" "public-subnet" {
   cidr_block = "172.16.1.0/24"
   vpc_id     = aws_vpc.myvpc.id
   map_public_ip_on_launch = "true"
   availability_zone = "us-east-1a"
   tags = {
     Name = "MyPublic-tf-subnet"
   }

 }

resource "aws_subnet" "private-subnet" {
  cidr_block = "172.16.2.0/24"
  vpc_id     = aws_vpc.myvpc.id
  availability_zone = "us-east-1b"
  map_public_ip_on_launch = "false"
  tags = {
    Name = "MyPrivate-tf-subnet"
  }

}

## Create IGW
resource "aws_internet_gateway" "myigw" {
  vpc_id = aws_vpc.myvpc.id
  tags = {
    Name = "my-igw"
  }
}

## Create RT for IGW
resource "aws_route_table" "myrt" {
  vpc_id = aws_vpc.myvpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.myigw.id
  }
  tags = {
    Name = "My-Public-RT"
  }
}
## Route Table Association
resource "aws_route_table_association" "rta" {
  subnet_id = aws_subnet.public-subnet.id
  route_table_id = aws_route_table.myrt.id
}


